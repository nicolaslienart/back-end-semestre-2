<?php
/*
 *  Show movies from filters
 */

// headers
header("Content-Type: application/json; charset=UTF-8");

$method = strtolower($_SERVER['REQUEST_METHOD']);

if ($method !== 'post') {
    http_response_code(405);
    echo json_encode(array('message' => 'This method is not allowed.'));
    exit;
}

// include data
include_once "../data/data_movies.php";
require "../../check.php";

// response status
http_response_code(200);

$query = array();

if (isset($_POST["title"])) {
    $query["title"] = $_POST["title"];
}
if (isset($_POST["releaseDate"])) {
    $query["releaseDate"] = $_POST["releaseDate"];
}
if (isset($_POST["director"])) {
    $query["director"] = $_POST["director"];
}
if (isset($_POST["genres"])) {
    $query["genres"] = json_decode($_POST["genres"], JSON_OBJECT_AS_ARRAY);
    array_walk($query["genres"], 'movieSearch_processId');
}

$output = array();


foreach ($movies as $key => $movie) {

    if (array_key_exists("title", $query)) {
        if (!movieSearch_strMatch($query["title"], $movie["title"])) {
            continue;
        }
    }

    if (array_key_exists("director", $query)) {
        if (!movieSearch_strMatch($query["director"], $movie["director"])) {
            continue;
        }
    }

    if (array_key_exists("releaseDate", $query)) {
        if (!movieSearch_releasedAfter($query['releaseDate'], $movie["date"])) {
            continue;
        }
    }
    if (array_key_exists("genres", $query)) {
        if (in_array("All", $query['genres']) === false) {
            if (!movieSearch_genresMatch($query['genres'], $movie["genre"])) {
                continue;
            }
        }
    }

    array_push($output, $movie);
}


echo json_encode($output);
exit();
