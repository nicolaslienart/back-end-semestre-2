<?php
/*
 *  Show movie by id
 */

// headers
header("Content-Type: application/json; charset=UTF-8");

// -- TO DO - check HTTP method
$method = strtolower($_SERVER['REQUEST_METHOD']);
if ($method !== 'get') {
    http_response_code(405);
    echo json_encode(array('message' => 'This method is not allowed.'));
    exit;
}


// include data
include_once "../data/MyPDO.imac-movies.include.php";

if (isset($_GET['id'])) {
    $query['id'] = $_GET['id'];
}
else {
	http_response_code(404);
  echo json_encode("No ID provided.");
  exit();
}

$output = array();
$stmt = MyPDO::getInstance()->prepare(<<<SQL
	SELECT *
	FROM Movies
  WHERE Movies.id = :idmovie
  LIMIT 1;
SQL
);

$stmt->execute(['idmovie' => $query['id']]);

while (($row = $stmt->fetch()) !== false) {
	array_push($output, $row);
}

foreach ($output as $key => $movie) {
	$genres = array();
	$directors = array();
	$actors = array();
	$countries = array();

	$stmt = MyPDO::getInstance()->prepare(<<<SQL
		SELECT Genres.name FROM `Genres`
		INNER JOIN MovieGenres ON MovieGenres.id_genre = Genres.id
		INNER JOIN Movies ON Movies.id = MovieGenres.id_movie
		WHERE Movies.id = :idmovie;
SQL
	);
	$stmt->execute(['idmovie'=>$movie['id']]);
	while (($row = $stmt->fetch()) !== false) {
		array_push($genres, $row['name']);
	}

	$stmt = MyPDO::getInstance()->prepare(<<<SQL
		SELECT Countries.name FROM Countries
		INNER JOIN MovieCountries ON MovieCountries.code_country = Countries.code
		INNER JOIN Movies ON Movies.id = MovieCountries.id_movie		WHERE Movies.id = :idmovie;
SQL
);
$stmt->execute(['idmovie'=>$movie['id']]);
while (($row = $stmt->fetch()) !== false) {
	array_push($countries, $row['name']);
}

$stmt = MyPDO::getInstance()->prepare(<<<SQL
	SELECT CONCAT(Casts.firstname, " ", Casts.lastname) AS "directors" FROM Casts
	INNER JOIN Roles ON Roles.id_cast = Casts.id
	INNER JOIN Movies ON Movies.id = Roles.id_movie
	INNER JOIN Jobs ON Jobs.id = Roles.id_job
	WHERE Jobs.name LIKE "director"
	AND Movies.id = :idmovie;
SQL
);
$stmt->execute(['idmovie'=>$movie['id']]);
while (($row = $stmt->fetch()) !== false) {
array_push($directors, $row['directors']);
}

$stmt = MyPDO::getInstance()->prepare(<<<SQL
	SELECT CONCAT(Casts.firstname, " ", Casts.lastname) AS "name", Roles.role FROM Casts
	INNER JOIN Roles ON Roles.id_cast = Casts.id
	INNER JOIN Movies ON Movies.id = Roles.id_movie
	INNER JOIN Jobs ON Jobs.id = Roles.id_job
	WHERE Jobs.name LIKE "actor"
	AND Movies.id = :idmovie;
SQL
);
$stmt->execute(['idmovie'=>$movie['id']]);
while (($row = $stmt->fetch()) !== false) {
	$actor = array('name' => $row['name'], 'role' => $row['role']);
array_push($actors, $actor);
}


	$output[$key]['genres'] = $genres;
	$output[$key]['countries'] = $countries;
	$output[$key]['directors'] = $directors;
	$output[$key]['actors'] = $actors;
}

if (empty($output)) {
  http_response_code(404);
  $output = "Cannot found movie with id {$query['id']}.";
}
else {
  http_response_code(200);
}

echo json_encode($output);

exit();
