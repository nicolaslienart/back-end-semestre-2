<?php
/*
 *  Add a movie
 */

// headers
header("Content-Type: application/json; charset=UTF-8");

function loadClass($class) {
  require "../../class/".strtolower($class).".class.php";
}

spl_autoload_register('loadClass');

// -- TO DO - check HTTP method
$method = strtolower($_SERVER['REQUEST_METHOD']);
if ($method !== 'post') {
    http_response_code(405);
    echo json_encode(array('message' => 'This method is not allowed.'));
    exit;
}


// include data
include_once "../data/MyPDO.imac-movies.include.php";

if (isset($_POST['title'])) {
    $query["title"] = $_POST['title'];
}
if (isset($_POST["releaseDate"])) {
    $date = new DateTime($_POST["releaseDate"]);
    $query["releaseDate"] = $date->format("Y-m-d");
}
if (isset($_POST["director"])) {
    $query["director"] = json_decode($_POST["director"], JSON_OBJECT_AS_ARRAY);
}
if (isset($_POST["genres"])) {
    $query["genres"] = json_decode($_POST["genres"], JSON_OBJECT_AS_ARRAY);
}
if (isset($_POST["actors"])) {
    $query["actors"] = json_decode($_POST["actors"], JSON_OBJECT_AS_ARRAY);
}
if (isset($_POST["countries"])) {
    $query["countries"] = json_decode($_POST["countries"], JSON_OBJECT_AS_ARRAY);
}

$movie;
if (array_key_exists('title', $query)) {

  if (array_key_exists('releaseDate', $query)) {
	  $movie = new Movies($query['title'], $query['releaseDate']);
  }
  else {
	  $movie = new Movies($query['title']);
  }
  $movie->addMovietoDB();
}
if (array_key_exists('actors', $query)) {
  $castID;
  foreach ($query['actors'] as $key => $actor) {
    if (($idCast = Casts::findCastInDB($actor['firstname'], $actor['lastname'])) !== NULL) {
      $castID = $idCast;
    }
    else {
      $cast = new Casts($actor['firstname'], $actor['lastname']);
      if (isset($actor['birth_date'])) {
        $cast->setBirthDate($actor['birth_date']);
      }
      if (isset($actor['death_date'])) {
        $cast->setDeathDate($actor['death_date']);
      }

      $cast->addCastToDB();
      $castID = $cast->getIdCast();
    }

    $role = new Roles($movie->getIdMovie(), $castID, 1);
    if (isset($actor['role'])) {
      $role->setRole($actor['role']);
    }

    $role->addRoleToDB();
  }
}

if (array_key_exists('directors', $query)) {
  $castID;
  foreach ($query['directors'] as $key => $director) {
    if (($idCast = Casts::findCastInDB($director['firstname'], $director['lastname'])) !== NULL) {
      $castID = $idCast;
    }
    else {
      $cast = new Casts($director['firstname'], $director['lastname']);
      if (isset($director['birth_date'])) {
        $cast->setBirthDate($director['birth_date']);
      }
      if (isset($director['death_date'])) {
        $cast->setDeathDate($director['death_date']);
      }

      $cast->addCastToDB();
      $castID = $cast->getIdCast();
    }

    $role = new Roles($movie->getIdMovie(), $castID, 2);

    $role->addRoleToDB();
  }
}

if (array_key_exists('countries', $query)) {
  $countryCODE;
  foreach ($query['countries'] as $key => $country) {
    if (($codeCountry = Countries::findCountryInDB($country['code'])) !== NULL) {
      $countryCODE = $codeCountry;
    }
    else {
      $newCountry = new Countries($country['name'], $country['code']);

      $newCountry->addCountryToDB();
      $countryCODE = $newCountry->getCodeCountry();
    }

    $movieCountry = new MovieCountries($movie->getIdMovie(), $countryCODE);

    $movieCountry->addMovieCountryToDB();
  }
}


/*
if(array_key_exists('title', $query)) {
  $stmt = MyPDO::getInstance()->prepare(<<<SQL
  INSERT INTO Movies (title)
  VALUES (:title_movie);
SQL
);

  $check = $stmt->execute(['title_movie' => $query['title']]);
  if (!$check) {
    echo json_encode("An error occured.");
    http_response_code(500);
    exit();
  }

  $idMovie = MyPDO::getInstance()->lastInsertId();

  if(array_key_exists('releaseDate', $query)) {
    $stmt = MyPDO::getInstance()->prepare(<<<SQL
      UPDATE Movies
      SET release_date = :date_movie
      WHERE Movies.id = :id_movie;
SQL
    );

  $stmt->execute(['id_movie' => $idMovie, 'date_movie' => $query['releaseDate']]);
  */
  /*
  if (array_key_exists('directors', $query)) {

      foreach ($query['directors'] as $key => $director) {
        $stmt = MyPDO::getInstance()->prepare(<<<SQL
          SELECT * FROM Casts
          WHERE lastname LIKE :director_lname
          AND firstname LIKE :director_fname;
SQL
);
        $stmt->execute(['director_lname' => $director['lastname'], 'director_fname' =>  $director['firstname']]);

        $directorDB;
        while ($row = $stmt->fetch()) {
            $directorDB = $row;
        }
        if ($directorDB) {
          $stmt = MyPDO::getInstance()->prepare(<<<SQL
            INSERT INTO Roles (id_movie, id_cast, id_job)
            VALUES (:id_movie, :id_director, 1);
SQL
);
          $stmt->execute(['id_movie' => $idMovie, 'id_director' =>  $directorDB['id']]);
        }

        $stmt = MyPDO::getInstance()->prepare(<<<SQL
          INSERT INTO Casts (firstname, lastname)
          VALUES (:title_movie);
SQL
);
      }
  }
  */
/*
  }
}
else {
  http_response_code(406);
  echo json_encode("Can't add movie to db due to missing title.");
  exit();
}
*/

/*
foreach ($output as $key => $movie) {
	$genres = array();
	$directors = array();
	$actors = array();
	$countries = array();

	$stmt = MyPDO::getInstance()->prepare(<<<SQL
		SELECT Genres.name FROM `Genres`
		INNER JOIN MovieGenres ON MovieGenres.id_genre = Genres.id
		INNER JOIN Movies ON Movies.id = MovieGenres.id_movie
		WHERE Movies.id = :idmovie;
SQL
	);
	$stmt->execute(['idmovie'=>$movie['id']]);
	while (($row = $stmt->fetch()) !== false) {
		array_push($genres, $row['name']);
	}

	$stmt = MyPDO::getInstance()->prepare(<<<SQL
		SELECT Countries.name FROM Countries
		INNER JOIN MovieCountries ON MovieCountries.code_country = Countries.code
		INNER JOIN Movies ON Movies.id = MovieCountries.id_movie		WHERE Movies.id = :idmovie;
SQL
);
$stmt->execute(['idmovie'=>$movie['id']]);
while (($row = $stmt->fetch()) !== false) {
	array_push($countries, $row['name']);
}

$stmt = MyPDO::getInstance()->prepare(<<<SQL
	SELECT CONCAT(Casts.firstname, " ", Casts.lastname) AS "directors" FROM Casts
	INNER JOIN Roles ON Roles.id_cast = Casts.id
	INNER JOIN Movies ON Movies.id = Roles.id_movie
	INNER JOIN Jobs ON Jobs.id = Roles.id_job
	WHERE Jobs.name LIKE "director"
	AND Movies.id = :idmovie;
SQL
);
$stmt->execute(['idmovie'=>$movie['id']]);
while (($row = $stmt->fetch()) !== false) {
array_push($directors, $row['directors']);
}

$stmt = MyPDO::getInstance()->prepare(<<<SQL
	SELECT CONCAT(Casts.firstname, " ", Casts.lastname) AS "name", Roles.role FROM Casts
	INNER JOIN Roles ON Roles.id_cast = Casts.id
	INNER JOIN Movies ON Movies.id = Roles.id_movie
	INNER JOIN Jobs ON Jobs.id = Roles.id_job
	WHERE Jobs.name LIKE "actor"
	AND Movies.id = :idmovie;
SQL
);
$stmt->execute(['idmovie'=>$movie['id']]);
while (($row = $stmt->fetch()) !== false) {
	$actor = array('name' => $row['name'], 'role' => $row['role']);
array_push($actors, $actor);
}


	$output[$key]['genres'] = $genres;
	$output[$key]['countries'] = $countries;
	$output[$key]['directors'] = $directors;
	$output[$key]['actors'] = $actors;
}
*/

/*
if (empty($output)) {
  http_response_code(404);
  $output = "Cannot found movie with id {$query['id']}.";
}
else {
  http_response_code(200);
}

echo json_encode($output);
*/

http_response_code(200);
echo json_encode("Success! movie id = {$movie->getIdMovie()}");

exit();
