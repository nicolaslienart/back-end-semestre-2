<?php
/*
 *  Show all movies
 */

// headers
header("Content-Type: application/json; charset=UTF-8");

// -- TO DO - check HTTP method
$method = strtolower($_SERVER['REQUEST_METHOD']);

if ($method !== 'get') {
	http_response_code(405);
	echo json_encode(array('message' => 'This method is not allowed.'));
	exit();
}

// include data
include_once "../data/MyPDO.imac-movies.include.php";

// response status
http_response_code(200);

$movies = array();
$stmt = MyPDO::getInstance()->prepare(`
SELECT *
FROM Movies
`);

$stmt->execute();

while (($row = $stmt->fetch()) !== false) {
	array_push($movies, $row);
}

foreach ($movies as $key => $movie) {
	$genres = array();
	$directors = array();
	$actors = array();
	$countries = array();

	$stmt = MyPDO::getInstance()->prepare(<<<SQL
		SELECT Genres.name FROM `Genres`
		INNER JOIN MovieGenres ON MovieGenres.id_genre = Genres.id
		INNER JOIN Movies ON Movies.id = MovieGenres.id_movie
		WHERE Movies.id = :idmovie;
SQL
	);
	$stmt->execute(['idmovie'=>$movie['id']]);
	while (($row = $stmt->fetch()) !== false) {
		array_push($genres, $row['name']);
	}

	$stmt = MyPDO::getInstance()->prepare(<<<SQL
		SELECT Countries.name FROM Countries
		INNER JOIN MovieCountries ON MovieCountries.code_country = Countries.code
		INNER JOIN Movies ON Movies.id = MovieCountries.id_movie		WHERE Movies.id = :idmovie;
SQL
);
$stmt->execute(['idmovie'=>$movie['id']]);
while (($row = $stmt->fetch()) !== false) {
	array_push($countries, $row['name']);
}

$stmt = MyPDO::getInstance()->prepare(<<<SQL
	SELECT CONCAT(Casts.firstname, " ", Casts.lastname) AS "directors" FROM Casts
	INNER JOIN Roles ON Roles.id_cast = Casts.id
	INNER JOIN Movies ON Movies.id = Roles.id_movie
	INNER JOIN Jobs ON Jobs.id = Roles.id_job
	WHERE Jobs.name LIKE "director"
	AND Movies.id = :idmovie;
SQL
);
$stmt->execute(['idmovie'=>$movie['id']]);
while (($row = $stmt->fetch()) !== false) {
array_push($directors, $row['directors']);
}

$stmt = MyPDO::getInstance()->prepare(<<<SQL
	SELECT CONCAT(Casts.firstname, " ", Casts.lastname) AS "name", Roles.role FROM Casts
	INNER JOIN Roles ON Roles.id_cast = Casts.id
	INNER JOIN Movies ON Movies.id = Roles.id_movie
	INNER JOIN Jobs ON Jobs.id = Roles.id_job
	WHERE Jobs.name LIKE "actor"
	AND Movies.id = :idmovie;
SQL
);
$stmt->execute(['idmovie'=>$movie['id']]);
while (($row = $stmt->fetch()) !== false) {
	$actor = array('name' => $row['name'], 'role' => $row['role']);
array_push($actors, $actor);
}


	$movies[$key]['genres'] = $genres;
	$movies[$key]['countries'] = $countries;
	$movies[$key]['directors'] = $directors;
	$movies[$key]['actors'] = $actors;
}
// send requested movie list
echo json_encode($movies);

exit();
