<?php
/*
 *  Show all genres in alphabetical order
 */

// headers
header("Content-Type: application/json; charset=UTF-8");

// check HTTP method
$method = strtolower($_SERVER['REQUEST_METHOD']);

if ($method !== 'get') {
	http_response_code(405);
	echo json_encode(array('message' => 'This method is not allowed.'));
	exit();
}

// include data
include_once "../data/MyPDO.imac-movies.include.php";

// response status
http_response_code(200);
$genres = array();

$stmt = MyPDO::getInstance()->prepare(<<<SQL
	SELECT name
	FROM Genres
SQL
);

$stmt->execute();

while (($row = $stmt->fetch()) !== false) {
	array_push($genres, $row['name']);
}


// show sorted array
sort($genres);
echo json_encode($genres);
exit();
