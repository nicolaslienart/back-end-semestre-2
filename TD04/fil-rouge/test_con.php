<?php
require_once "api/data/MyPDO.imac-movies.include.php";

$stmt = MyPDO::getInstance()->prepare(<<<SQL
	SELECT *
	FROM Countries
	ORDER BY code
SQL
);

$stmt->execute();

while (($row = $stmt->fetch()) !== false) {
	echo "<div>{$row['name']}</div>";
}
 ?>
