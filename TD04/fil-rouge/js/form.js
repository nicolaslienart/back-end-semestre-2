// document ready in ES6
Document.prototype.ready = callback => {
	if(callback && typeof callback === 'function') {
		document.addEventListener("DOMContentLoaded", () =>  {
			if(document.readyState === "interactive" || document.readyState === "complete") {
				return callback();
			}
		});
	}
};

// fill genres from data_genres.php
document.ready( () => {
	fetch("api/genres/read.php") // à corriger si cela ne fonctionne pas
		.then( response => response.json() )
		.then( data => {
			let genres = document.getElementById('list-genre');
			data.forEach( genre => {
				let radio = document.createElement("input");
				radio.type = "checkbox";
				radio.name = "genre";
				radio.value = genre;
				radio.id = "input-radio-" + genre.toLowerCase();

				let label = document.createElement("label")
				label.htmlFor = "input-radio-" + genre.toLowerCase();
				label.innerHTML = genre;

				let li  = document.createElement("li");
				li.appendChild(radio);
				li.appendChild(label);

				genres.appendChild(li);
			});
		})
		.catch(error => { console.log(error) });
});

function display_movieList(movie) {
	let list = document.querySelector('.results');
	let node = document.createElement("li");
	let textnode = document.createTextNode(movie.title);
	node.appendChild(textnode);
	list.appendChild(node);
}

document.getElementById('button-search').onclick = event => {
	event.preventDefault();


	let formData = new FormData();
  let data = new Object();


	let title = document.getElementById("input-title").value;
	let releaseDate = document.getElementById("input-date").value;
	let genres = document.querySelectorAll("input[name='genre']");
	let genreChecked = new Array();
	genres.forEach(function(radioBtn) {
		if (radioBtn.checked) {
			genreChecked.push(radioBtn.id);
		}
	});
	let director = document.getElementById("input-director").value;

	if (title) {
    data['title'] = title;
		formData.append("title", title);
	}

	if (releaseDate) {
    data['releaseDate'] = releaseDate;
		formData.append("releaseDate", releaseDate);
	}
	if (genreChecked) {
    data['genres'] = genreChecked;
		formData.append("genres", JSON.stringify(genreChecked));
	}
	if (director) {
    data['director'] = director;
		formData.append("director", director);
	}
  console.log(data);
  data = JSON.stringify(data);
  formData.append('postData',data);
  console.log(data);


	fetch("api/movies/search.php", {
		method: "POST",
		body: formData})
		.then( response => response.json() )
		.then( data => {
      let list = document.querySelector('.results');
      while (list.hasChildNodes()) {
        list.removeChild(list.firstChild);
      }

			console.log(data);
      if (data.length == 0) {
        list.appendChild(document.createTextNode("Nothing Found"));
      }
      else {
        data.forEach( movie => display_movieList(movie));
      }

		})
		.catch( error => {
      window.alert(error);
		});
};
