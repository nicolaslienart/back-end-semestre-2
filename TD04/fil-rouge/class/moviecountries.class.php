<?php
class MovieCountries
{
  private $_idMovie;
  private $_codeCountry;

  public function __construct($idMovie, $codeCountry)
  {
    $this->_idMovie = $idMovie;
    $this->_codeCountry = $codeCountry;
  }

  public function addMovieCountryToDB()
  {
    $stmt = MyPDO::getInstance()->prepare("INSERT INTO MovieCountries (id_movie, code_country) VALUES (?, ?);");
    $queryStatus = $stmt->execute(
      array(
        $this->_idMovie,
        $this->_codeCountry
      )
    );

    if ($queryStatus === false) {
      //TODO throwAnError
    }
  }

  public static function removeMovieCountryFromDB($idMovie, $codeCountry)
  {
    $queryStmt = "DELETE FROM MovieCountries WHERE code_country = ? AND id_movie = ?;";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        $codeCountry,
        $idMovie
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function editCodeMovieCountryInDB($idMovie, $codeCountry, $newCode)
  {
    $queryStmt = "UPDATE MovieCountries SET code_country = :newCode WHERE code_country = :codeCountry AND id_movie = :idMovie;";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        ':newCode' => $newCode,
        ':codeCountry' => $codeCountry,
        ':idMovie' => $idMovie,
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function editIdMovieCountryInDB($idMovie, $codeCountry, $newMovie)
  {
    $queryStmt = "UPDATE MovieCountries SET id_movie = :newMovie WHERE code_country = :codeCountry AND id_movie = :idMovie;";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        ':newMovie' => $newMovie,
        ':codeCountry' => $codeCountry,
        ':idMovie' => $idMovie,
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public function getMovieCountry()
  {
    return array(
      'idMovie' => $this->_idMovie,
      'codeCountry' => $this->_codeCountry,
      );
  }
}
 ?>
