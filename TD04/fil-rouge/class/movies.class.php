<?php
class Movies {
  private $_titleMovie;
  private $_releaseDate;
  private $_idMovie;

  public function __construct($titleMovie, $releaseDateMovie = NULL)
  {
      $this->_titleMovie = $titleMovie;
      $this->_releaseDate = $releaseDateMovie;
  }

  public function setMovieDate($releaseDateMovie)
  {
    $this->_releaseDate = $releaseDateMovie;
  }

  public static function findMovie($id)
  {
    $queryStmt = "SELECT * FROM Movies WHERE id = ?";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        $id
      )
    );

    if ($row = $pdostat->fetch()) {
      $movie = new Movies($row['title'], $row['release_date']);
      $movie->setMovieID($row['id']);
      return $movie;
    }
  }

  public function setMovieID($id)
  {
    $this->_idMovie = $id;
  }

  public function addMovieToDB() {

    $stmt = MyPDO::getInstance()->prepare("INSERT INTO Movies (title, release_date) VALUES (?, ?);");

    $stmt->bindValue(1, $this->_titleMovie);
    $stmt->bindValue(2, $this->_releaseDate);

    $queryStatus = $stmt->execute();

    if ($queryStatus === false) {
      self::throwAnError();
    }
    else {
      $this->_idMovie = MyPDO::getInstance()->lastInsertId();
    }
  }

  public static function removeMovieFromDB($id)
  {
    $queryStmt = "DELETE FROM Movies WHERE id = ?";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        $id
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function editTitleMovieInDB($id, $title)
  {
    $queryStmt = "UPDATE Movies SET title = :title WHERE id = :id";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        ':title' => $title,
        ':id' => $id
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function editRealeaseDateMovieInDB($id, $releaseDate)
  {
    $queryStmt = "UPDATE Movies SET release_date = :date WHERE id = :id";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        ':date' => $releaseDate,
        ':id' => $id
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  // TODO: put throwAnError elsewhere
  public static function throwAnError()
  {
    echo json_encode("An error occured.");
    http_response_code(500);
    exit();
  }

  public function getIdMovie()
  {
    return $this->_idMovie;
  }

}
 ?>
