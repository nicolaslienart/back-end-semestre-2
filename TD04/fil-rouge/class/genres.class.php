<?php
class Genres
{
  private $_id;
  private $_name;

  public function __construct($name)
  {
    $this->_name = $name;
  }

  public function setGenreID($id)
  {
    $this->_id = $id;
  }

  public function addGenreToDB()
  {
    $stmt = MyPDO::getInstance()->prepare("INSERT INTO Genres (name) VALUES (?);");
    $queryStatus = $stmt->execute(
      array(
        $this->_name,
      )
    );

    if ($queryStatus === false) {
      //TODO throwAnError
    }
  }

  public static function removeGenreFromDB($name)
  {
    $queryStmt = "DELETE FROM Genres WHERE name LIKE ?";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        $name
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function editnameGenreInDB($name, $id)
  {
    $queryStmt = "UPDATE Genres SET name = :name WHERE id = :id";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        ':name' => $name,
        ':id' => $id
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function findGenreInDB($id)
  {
    $stmt = MyPDO::getInstance()->prepare("SELECT * FROM Genres WHERE id = ?;");

    $stmt->execute(
      array(
        $id
      )
    );

    if (($genre = $stmt->fetch()) !== FALSE) {
      $genre = new Genres($row['name']);
      $genre->setGenreID($row['id']);
      return $genre;
    }
    else {
      return NULL;
    }
  }

  public function getGenre()
  {
    return $this->_name;
  }
}
 ?>
