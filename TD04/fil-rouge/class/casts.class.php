<?php
class Casts
{
  private $_firstname;
  private $_lastname;
  private $_birthDate = NULL;
  private $_deathDate = NULL;

  private $_idCast;

  public function __construct($fnameCast, $lnameCast, $bdateCast = NULL, $ddateCast = NULL)
  {
    $this->_firstname = $fnameCast;
    $this->_lastname = $lnameCast;
    $this->_birthDate = $bdateCast;
    $this->_deathDate = $ddateCast;
  }

  public function setBirthDate($birthDate)
  {
    $this->_birthDate = $birthDate;
  }

  public function setDeathDate($deathDate)
  {
    $this->_deathDate = $deathDate;
  }

  public function addCastToDB()
  {
    $stmt = MyPDO::getInstance()->prepare("INSERT INTO Casts (firstname, lastname, birth_year, death_year) VALUES (?, ?, ?, ?);");
    $queryStatus = $stmt->execute(
      array(
        $this->_firstname,
        $this->_lastname,
        $this->_birthDate,
        $this->_deathDate
      )
    );

    if ($queryStatus === false) {
      //TODO throwAnError
    }
    else {
      $this->_idCast = MyPDO::getInstance()->lastInsertId();
    }
  }

  public static function removeCastFromDB($id)
  {
    $queryStmt = "DELETE FROM Casts WHERE id = ?";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        $id
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function editFirstnameCastInDB($id, $fnameCast)
  {
    $queryStmt = "UPDATE Casts SET firstname = :name WHERE id = :id";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        ':name' => $fnameCast,
        ':id' => $id
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function editLastnameCastInDB($id, $lnameCast)
  {
    $queryStmt = "UPDATE Casts SET lastname = :name WHERE id = :id";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        ':name' => $lnameCast,
        ':id' => $id
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function editBirthDateCastInDB($id, $birthDate)
  {
    $queryStmt = "UPDATE Casts SET birth_date = :date WHERE id = :id";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        ':date' => $birthDate,
        ':id' => $id
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function editDeathDateCastInDB($id, $deathDate)
  {
    $queryStmt = "UPDATE Casts SET death_date = :date WHERE id = :id";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        ':date' => $birthDate,
        ':id' => $id
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function findCastInDB($fnameCast, $lnameCast)
  {
    $stmt = MyPDO::getInstance()->prepare("SELECT * FROM Casts WHERE firstname LIKE :fname AND lastname LIKE :lname LIMIT 1;");

    // echo $stmt->queryString;

    $stmt->execute(
      array(
        ':fname' => $fnameCast,
        ':lname' => $lnameCast
      )
    );

    if (($cast = $stmt->fetch()) !== FALSE) {
      return $cast['id'];
    }
    else {
      return NULL;
    }
  }

  public function getIdCast()
  {
    return $this->_idCast;
  }
}
 ?>
