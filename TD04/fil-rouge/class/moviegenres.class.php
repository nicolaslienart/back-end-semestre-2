<?php
class MovieGenres
{
  private $_idMovie;
  private $_idGenre;

  public function __construct($idMovie, $idGenre)
  {
    $this->_idMovie = $idMovie;
    $this->_codeCountry = $codeCountry;
  }

  public function addGenreMovieToDB()
  {
    $stmt = MyPDO::getInstance()->prepare("INSERT INTO MovieCountries (id_movie, code_country) VALUES (?, ?);");
    $queryStatus = $stmt->execute(
      array(
        $this->_idMovie,
        $this->_idGenre
      )
    );

    if ($queryStatus === false) {
      //TODO throwAnError
    }
  }

  public static function removeGenreMovieFromDB($idMovie, $idGenre)
  {
    $queryStmt = "DELETE FROM MovieGenres WHERE id_movie = ? AND id_genre = ?;";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        $idMovie,
        $idGenre
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function editIDMovieGenreInDB($idMovie, $idGenre, $newIDMovie)
  {
    $queryStmt = "UPDATE MovieGenres SET id_movie = :newIDMovie WHERE id_genre = :idGenre AND id_movie = :idMovie;";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        ':newIDMovie' => $newIDMovie,
        ':idGenre' => $idGenre,
        ':idMovie' => $idMovie,
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function editIdMovieCountryInDB($idMovie, $codeCountry, $newMovie)
  {
    $queryStmt = "UPDATE MovieCountries SET id_movie = :newMovie WHERE code_country = :codeCountry AND id_movie = :idMovie;";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        ':newMovie' => $newMovie,
        ':codeCountry' => $codeCountry,
        ':idMovie' => $idMovie,
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public function getMovieGenre()
  {
    return array(
      'idMovie' => $this->_idMovie,
      'idGenre' => $this->_idGenre,
      );
  }
}
 ?>
