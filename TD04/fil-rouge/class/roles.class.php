<?php
class Roles
{
  private $_idMovie;
  private $_idCast;
  private $_idJob;
  private $_role = NULL;

  public function __construct($idMovie, $idCast, $idJob, $role = NULL)
  {
    $this->_idMovie = $idMovie;
    $this->_idCast = $idCast;
    $this->_idJob = $idJob;
    $this->_role = $role;
  }

  public function setRole($role)
  {
    $this->_role = $role;
  }

  public function addRoleToDB()
  {
    $stmt = MyPDO::getInstance()->prepare("INSERT INTO Roles (id_movie, id_cast, id_job, role) VALUES (?, ?, ?, ?);");
    $queryStatus = $stmt->execute(
      array(
        $this->_idMovie,
        $this->_idCast,
        $this->_idJob,
        $this->_role
      )
    );

    if ($queryStatus === false) {
      //TODO throwAnError
    }
  }

  public static function removeRoleFromDB($id, $attribute = 'idMovie')
  {
    switch ($attribute) {
      default:
      $queryStmt = "DELETE FROM Roles WHERE id_movie = ?";
        break;

      case 'idCast':
      $queryStmt = "DELETE FROM Roles WHERE id_cast = ?";
        break;
    }

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        $id
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function editRoleNameRoleInDB($idMovie, $idCast, $idJob, $role)
  {
    $queryStmt = "UPDATE Roles SET role = :role WHERE id_movie = :idMovie AND id_cast = :idCast AND id_job = :idJob;";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        ':idMovie' => $idMovie,
        ':idCast' => $idCast,
        ':idJob' => $idJob,
        ':role' => $role
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function editIdMovieRoleInDB($idMovie, $idCast, $idJob, $newIdMovie)
  {
    $queryStmt = "UPDATE Roles SET id_movie = :idP WHERE id_movie = :idMovie AND id_cast = :idCast AND id_job = :idJob;";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        ':idMovie' => $idMovie,
        ':idCast' => $idCast,
        ':idJob' => $idJob,
        ':idP' => $newIdMovie
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function editIdCastRoleInDB($idMovie, $idCast, $idJob, $newIdCast)
  {
    $queryStmt = "UPDATE Roles SET id_cast = :idP WHERE id_movie = :idMovie AND id_cast = :idCast AND id_job = :idJob;";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        ':idMovie' => $idMovie,
        ':idCast' => $idCast,
        ':idJob' => $idJob,
        ':idP' => $newIdCast
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function editIdJobRoleInDB($idMovie, $idCast, $idJob, $newIdJob)
  {
    $queryStmt = "UPDATE Roles SET id_job = :idP WHERE id_movie = :idMovie AND id_cast = :idCast AND id_job = :idJob;";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        ':idMovie' => $idMovie,
        ':idCast' => $idCast,
        ':idJob' => $idJob,
        ':idP' => $newIdJob
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public function getIdCast()
  {
    return array(
      'idMovie' => $_idMovie,
      'idCast' => $_idCast,
      'idJob' => $_idJob
      );
  }
}
 ?>
