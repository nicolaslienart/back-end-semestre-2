<?php
class Countries
{
  private $_name;
  private $_code;

  public function __construct($name, $code)
  {
    $this->_name = $name;
    $this->_code = $code;
  }

  public function addCountryToDB()
  {
    $stmt = MyPDO::getInstance()->prepare("INSERT INTO Countries (name, code) VALUES (?, ?);");
    $queryStatus = $stmt->execute(
      array(
        $this->_name,
        $this->_code
      )
    );

    if ($queryStatus === false) {
      //TODO throwAnError
    }
  }

  public static function removeCountryFromDB($code)
  {
    $queryStmt = "DELETE FROM Countries WHERE code = ?";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        $code
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function editCodeCountryInDB($code, $newCode)
  {
    $queryStmt = "UPDATE Countries SET code = :newCode WHERE code = :code";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        ':newCode' => $newCode,
        ':code' => $code
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function editNameCountryInDB($code, $newName)
  {
    $queryStmt = "UPDATE Countries SET name = :name WHERE code = :code";

    $stmt = MyPDO::getInstance()->prepare($queryStmt);
    $stmt->execute(
      array(
        ':name' => $newName,
        ':code' => $code
      )
    );

    if ($stmt->rowCount() == 0) {
      return NULL;
    }
  }

  public static function findCountryInDB($code)
  {
    $stmt = MyPDO::getInstance()->prepare("SELECT * FROM Countries WHERE code = ? LIMIT 1;");

    $stmt->execute(
      array(
        $code
      )
    );

    if (($country = $stmt->fetch()) !== FALSE) {
      return $country['code'];
    }
    else {
      return NULL;
    }
  }

  public function getCodeCountry()
  {
    return $this->_code;
  }
}
 ?>
