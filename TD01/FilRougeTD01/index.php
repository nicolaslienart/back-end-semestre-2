<?php
  include("data_movies.php");
  require("render_movie_list.php");
 ?>
<!DOCTYPE html>
<html lang="fr-FR" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>A l'affiche cette semaine</title>
    <style media="screen">
      body {
        background-color: rgb(44, 52, 60);
        color: white;
        font-family: sans-serif;
      }
      h1 {
        text-align: center;
        font-size: 3em;
        text-transform: uppercase;
        text-shadow: 0 0 13px rgb(0, 0, 0);
      }
      .container {
        display: grid;
        grid-template-columns: 1fr 1fr;
        grid-gap: 30px;
        width: 90%;
        margin: auto;
      }
      .container div {
        background-color: rgb(69, 49, 108);
        padding: 10px;
        border-radius: 10px;
        box-shadow: 0 0 13px rgba(208, 248, 8, 0.5);
        font-size: smaller;
        border: dotted 9px yellow;
        text-align: center;
      }
      h2 {
      }
      h2:first-child {
        font-size: 1.7em;
      }
    </style>
  </head>
  <body>
    <h1>A l'affiche cette semaine</h1>
    <div class="container">
      <?php
      if ($movies) {
          render_movie_list($movies);
      }
       ?>
    </div>

  </body>
</html>
