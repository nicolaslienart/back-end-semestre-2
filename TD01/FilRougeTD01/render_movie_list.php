<?php

function print_label($titre, $data) {

    echo ucfirst($titre);
    if (gettype($data) == "array") {
      echo "s";
    }
    echo " : ";

}

function print_array($data) {

  if (gettype($data) == "array") {
    foreach ($data as $key => $value) {
      echo "$value";
      if ($key != count($data) - 1) {
        echo ", ";
      }
      else {
        echo ".";
      }
    }

  }
  else {
    echo "$data.";
  }

}

function get_date($date) {

  $start = stripos($date,'-') + 1;
  $size = 2;
  $date = substr($date,  $start, $size);

  return (int) $date;

}

function render_movie_list($movies) {

  foreach ($movies as $key => $value)
  {
    echo "<div>";
    echo "<h2>$value[title]</h2>";

    $dateColor = "default";
    $dateEmoji = "";
    if (get_date($value['date']) == 02) {
      $dateEmoji = "📅";
      $dateColor = "red";
    }
    echo "<h2 style='color:$dateColor;'>$value[date] $dateEmoji</h2>";

    $genreDate = "default";
    if ($value['genre'] == "Comedy") {
      $genreDate = "green";
    }
    elseif (gettype($value['genre']) == "array") {
      if (in_array("Comedy", $value['genre'])) {
        $genreDate = "green";
      }
    }

    echo "<h3 style='color:".$genreDate.";'>";
    print_label("genre", $value['genre']);
    print_array($value['genre']);
    echo "</h3>";
    echo "<h3>";
    print_label("réalisateur", $value['director']);
    print_array($value['director']);
    echo "</h3>";
    echo "</div>";
  }

}
 ?>
