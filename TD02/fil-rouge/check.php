<?php
  include "api/data/data_genres.php";
  function movieSearch_cleanStr($inputStr)
  {
      $cleanString = strtolower($inputStr);
      $cleanString = str_replace([' ', ':'], '-', $cleanString);
      $cleanString = preg_replace('/-+/', '-', $cleanString);

      return $cleanString;
  }

  function movieSearch_releasedAfter($dateUsr, $dateMovie)
  {
      $date_query = new DateTime($dateUsr);
      $date_movie = new DateTime($dateMovie);

      $date_diff = $date_query->diff($date_movie);

      if (!$date_diff->invert) {
          return true;
      } else {
          return false;
      }
  }

  function movieSearch_strMatch($strUsr, $strMovie)
  {
      if (is_array($strMovie)) {
          $found = false;
          foreach ($strMovie as $key => $value) {
              if (movieSearch_strMatch($strUsr, $value)) {
                  $found = true;
              }
          }
          return $found;
      } else {
          $str_query = movieSearch_cleanStr($strUsr);
          $str_movie = movieSearch_cleanStr($strMovie);

          if (strpos($str_movie, $str_query) !== false) {
              return true;
          } else {
              return false;
          }
      }
  }

  function movieSearch_processId(&$item, $key)
  {
      $item = str_replace("input-radio-", "", $item);
      $item = ucfirst($item);
  }

  function movieSearch_genresMatch($genresUsr, $genresMovie)
  {
      if (!is_array($genresMovie)) {
          $genresMovie = array(0 => $genresMovie);
      }

      foreach ($genresUsr as $key => $genres_query) {
          if ($genres_query == "All") {
              continue;
          }
          if (in_array($genres_query, $genresMovie) === false) {
              return false;
          }
      }
      return true;
  }
