<?php
/*
 *  Show all movies
 */

// headers
header("Content-Type: application/json; charset=UTF-8");

// -- TO DO - check HTTP method
$method = strtolower($_SERVER['REQUEST_METHOD']);

if ($method !== 'post') {
	http_response_code(405);
	echo json_encode(array('message' => 'This method is not allowed.'));
	exit();
}

// include data
include_once "../data/data_movies.php";

// response status
http_response_code(200);

// send requested movie list
echo json_encode($movies);

exit();
