<?php
/*
 *  Show movie by id
 */

// headers
header("Content-Type: application/json; charset=UTF-8");

// -- TO DO - check HTTP method
if ($method !== 'get') {
    http_response_code(405);
    echo json_encode(array('message' => 'This method is not allowed.'));
    exit;
}


// include data
include_once "../data/data_movies.php";

// -- TODO - check params
if (isset($_GET['id'])) {
    $query['id'] = $_GET['id'];
}

// -- TODO - response
if ($query['id'] && $query['id'] < count($movies)) {
  $output = $movies[$query['id']];
}
else {
  $output = 'NULL';
}

echo json_encode($output);

exit();
